from itertools import cycle
from geopy import Point
from geopy.distance import distance

def generate_grid_points():
    """Generate a grid of intersection points between lines of latitude
    and lines of longitude."""
    grid_points = []

    for lat in xrange(-90, 91, lat_res):
        for lng in xrange(-179, 181, lng_res):
            point = Point(lat, lng)
            grid_points.append(point)

    return grid_points

def closest_grid_point(point):
    """Find the grid point closest to the given point."""
    pass


def generate_neighbours(point):
    """Generate neighbours for given points, taking into account
    that polar coordinates wrap"""

    lat_rng = [b for b in xrange(-90, 91)]
    lng_rng = [b for b in xrange(-179, 181)]
    lat_idx = lat_rng.index(point.latitude)
    lng_idx = lng_rng.index(point.longitude)

    # TODO this is pretty ugly and should be fixed
    return [
        Point(lat_rng[(lat_idx + 1) % len(lat_rng)], point.longitude),
        Point(lat_rng[(lat_idx - 1) % len(lat_rng)], point.longitude),
        Point(point.latitude, lng_rng[(lng_idx + 1) % len(lng_rng)]),
        Point(point.latitude, lng_rng[(lng_idx - 1) % len(lng_rng)])
    ]

def generate_grid_graph(grid_points):
    """Generate a graph to use for pathfinding."""
    graph = {}
    for point in grid_points:
        graph[point] = generate_neighbours(point)

    return graph
